## Homework Project 5, A Bug's Life

### 100 Points

### Due: Wednesday August 11, 2021 at 11:59 p.m. (No Late Submission)

---
### *** IMPORTANT ***
#### Do not delete or modify any of the files in this repository other than "book.cc", "book.h", book-main.cc and this "README.md" files.

---

### Name: Eric Peters

### Email: ep009819@ohio.edu

---

### Assignment: [05-hw-wasps-ants.pdf](05-hw-wasps-ants.pdf)

### Style: Please follow our coding style: [Coding Style](https://github.com/nasseef/cs2400/blob/master/docs/coding-style.md)

---

### Instructions

- Clone this repository on your machine

- Update this file `README.md` by entering your information above.
- Write/update your program

- Compile and run your program to test it

- Commit regularly

- Push your repository to GitHub (often)

---

### Submitting your assignment

**Submit a link to your repository on Blackboard. Make sure this is done before the due date.**
---