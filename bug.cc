/**
 *    @file: bug.cpp
 *  @author: Eric Peters
 *    @date: 2021-08-4, 9:13PM
 *   @brief: implementation for the bug class
 */

#include "bug.h"
#include <cstdlib>

using namespace std;

Bug::Bug() {
    steps = 0;
    symbol = ' ';
}

Bug::Bug(size_t row, size_t col) {
    Bug::row = row;
    Bug::col = col;
    steps = 0;
    symbol = ' ';
}
