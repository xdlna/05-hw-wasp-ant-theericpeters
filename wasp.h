/**
 *   @file: wasp.h
 * @author: Nasseef Abukamail
 *   @date: July 28, 2021
 *  @brief: Wasp class
 */
#ifndef WASP_H
#define WASP_H

#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "bug.h"

using namespace std;

class Wasp : public Bug {
public:
    Wasp();

    Wasp(size_t row, size_t col);

    virtual ~Wasp();

    void move(Bug *grid[][GRID_SIZE]) override;
    void breed(Bug *grid[][GRID_SIZE]) override;

private:
    size_t starve{};
};

#endif  // WASP_H