/**
 *    @file: wasp.cc
 *  @author: Eric Peters
 *    @date: 2021-08-9, 4:11PM
 *   @brief: Implementation for the wasp class.
 */
#include "wasp.h"

using namespace std;

Wasp::Wasp() {
    symbol = '&';
}

Wasp::Wasp(size_t row, size_t col) : Bug(row, col) {
    symbol = '&';
}

Wasp::~Wasp() {}

void Wasp::move(Bug *(*grid)[20]) {
    if (row != 19) {
        if (grid[row + 1][col] != nullptr) {
            if (grid[row + 1][col]->getSymbol() == ';') {
                grid[row + 1][col] = grid[row][col];
                grid[row][col] = nullptr;
                starve = 0;
                row++;
                steps++;
            }
        }
    } else if (col != 19) {
        if (grid[row][col + 1] != nullptr) {

            if (grid[row][col + 1]->getSymbol() == ';') {
                grid[row][col + 1] = grid[row][col];
                grid[row][col] = nullptr;
                starve = 0;
                row++;
                steps++;
            }
        }
    } else if (row != 0) {
        if (grid[row - 1][col] != nullptr) {
            if (grid[row - 1][col]->getSymbol() == ';') {
                grid[row - 1][col] = grid[row][col];
                grid[row][col] = nullptr;
                starve = 0;
                row++;
                steps++;
            }
        }
    } else if (col != 0) {
        if (grid[row][col - 1] != nullptr) {
            if (grid[row][col - 1]->getSymbol() == ';') {
                grid[row][col - 1] = grid[row][col];
                grid[row][col] = nullptr;
                starve = 0;
                row++;
                steps++;
            }
        }
    }


    int direction = rand() % 3;
    switch (direction) {
        case 0:
            if (row != 19)
                if (grid[row + 1][col] == nullptr) {
                    grid[row + 1][col] = grid[row][col];
                    grid[row][col] = nullptr;
                    row++;
                    steps++;
                    starve++;
                }
        case 1:
            if (col != 19)
                if (grid[row][col + 1] == nullptr) {
                    grid[row][col + 1] = grid[row][col];
                    grid[row][col] = nullptr;
                    col++;
                    steps++;
                    starve++;
                }
        case 2:
            if (row != 0)
                if (grid[row - 1][col] == nullptr) {
                    grid[row - 1][col] = grid[row][col];
                    grid[row][col] = nullptr;
                    row--;
                    steps++;
                    starve++;
                }
        case 3:
            if (col != 0)
                if (grid[row][col - 1] == nullptr) {
                    grid[row][col - 1] = grid[row][col];
                    grid[row][col] = nullptr;
                    col--;
                    steps++;
                    starve++;
                }
    }
    srand(time(nullptr));
}

void Wasp::breed(Bug *(*grid)[20]) {
    int direction = rand() % 3;
    switch (direction) {
        case 0:
            if (row != 19)
                if (grid[row + 1][col] == nullptr) {
                    grid[row + 1][col] = new Wasp(row + 1, col);
                    steps = 0;
                }
        case 1:
            if (col != 19)
                if (grid[row - 1][col] == nullptr) {
                    grid[row - 1][col] = new Wasp(row - 1, col);
                    steps = 0;
                }
        case 2:
            if (row != 0)
                if (grid[row][col + 1] == nullptr) {
                    grid[row][col + 1] = new Wasp(row, col + 1);
                    steps = 0;
                }
        case 3:
            if (col != 0)
                if (grid[row][col - 1] == nullptr) {
                    grid[row][col - 1] = new Wasp(row, col - 1);
                    steps = 0;
                }
    }
    srand(time(nullptr));
}
