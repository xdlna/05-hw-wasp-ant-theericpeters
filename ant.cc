/**
 *    @file: ant.cc
 *  @author: Eric Peters
 *    @date: 2021-08-9, 4:11PM
 *   @brief: Implementation for the ant class.
 */
#include "ant.h"

using namespace std;

Ant::Ant() {
    symbol = ';';
}

Ant::Ant(size_t row, size_t col) : Bug(row, col) {
    symbol = ';';
}

Ant::~Ant() {}

void Ant::move(Bug *grid[][GRID_SIZE]) {
    int direction = rand() % 3;
    switch (direction) {
        case 0:
            if (row != 19)
                if (grid[row + 1][col] == nullptr) {
                    grid[row + 1][col] = grid[row][col];
                    grid[row][col] = nullptr;
                    row++;
                    steps++;
                }
        case 1:
            if (col != 19)
                if (grid[row][col + 1] == nullptr) {
                    grid[row][col + 1] = grid[row][col];
                    grid[row][col] = nullptr;
                    col++;
                    steps++;
                }
        case 2:
            if (row != 0)
                if (grid[row - 1][col] == nullptr) {
                    grid[row - 1][col] = grid[row][col];
                    grid[row][col] = nullptr;
                    row--;
                    steps++;
                }
        case 3:
            if (col != 0)
                if (grid[row][col - 1] == nullptr) {
                    grid[row][col - 1] = grid[row][col];
                    grid[row][col] = nullptr;
                    col--;
                    steps++;
                }
    }
    srand(time(nullptr));
}

void Ant::breed(Bug *grid[][GRID_SIZE]) {
    int direction = rand() % 3;
    switch (direction) {
        case 0:
            if (row != 19)
                if (grid[row + 1][col] == nullptr) {
                    grid[row + 1][col] = new Ant(row + 1, col);
                    steps = 0;
                }
        case 1:
            if (col != 19)
                if (grid[row - 1][col] == nullptr) {
                    grid[row - 1][col] = new Ant(row - 1, col);
                    steps = 0;
                }
        case 2:
            if (row != 0)
                if (grid[row][col + 1] == nullptr) {
                    grid[row][col + 1] = new Ant(row, col + 1);
                    steps = 0;
                }
        case 3:
            if (col != 0)
                if (grid[row][col - 1] == nullptr) {
                    grid[row][col - 1] = new Ant(row, col - 1);
                    steps = 0;
                }
    }
    srand(time(nullptr));
}
