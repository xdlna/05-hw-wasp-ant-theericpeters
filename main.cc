/**
 *   @file: predator-prey.cc
 * @author: Nasseef Abukamail
 *   @date: July 28, 2021
 *  @brief: Predator Pray game of wasps and ants.
 */

#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "ant.h"
#include "bug.h"
#include "wasp.h"

using namespace std;

/// Constants and function prototypes
const size_t NUM_WASPS = 5;
const size_t NUM_ANTS = 100;

void initializeGrid(Bug *grid[][GRID_SIZE]);

void displayGrid(Bug *grid[][GRID_SIZE]);

void timeStep(Bug *grid[][GRID_SIZE]);

int main(int argc, char const *argv[]) {
    Bug *grid[GRID_SIZE][GRID_SIZE];
    srand(time(nullptr));

    initializeGrid(grid);
    string ans = "";
    do {
        displayGrid(grid);
        timeStep(grid);
        cout << "hit enter to move again, type n to quit: ";
        getline(cin, ans);
    } while (ans == "");

    //if you want, you run the simulation a certain number of times
    //uncomment the following
    /*
    displayGrid(grid);
    for (size_t i = 0; i < 1000; i++)
    {
        timeStep(grid);
    }
    displayGrid(grid);
    */
    return 0;
}  /// main

void initializeGrid(Bug *grid[][GRID_SIZE]) {
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 20; ++j) {
            grid[i][j] = nullptr;
        }
    }

    for (int i = 0; i < 5; ++i) {
        int row = rand() % 20;
        int col = rand() % 20;

        if (grid[row][col] == nullptr)
            grid[row][col] = new Wasp(row, col);
        else
            i--;
    }

    for (int i = 0; i < 100; ++i) {
        int row = rand() % 20;
        int col = rand() % 20;

        if (grid[row][col] == nullptr)
            grid[row][col] = new Ant(row, col);
        else
            i--;
    }
} /// initializeGrid

void displayGrid(Bug *grid[][GRID_SIZE]) {
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 20; ++j) {
            if (grid[i][j] == nullptr)
                cout << "  ";
            else
                cout << " " << grid[i][j]->getSymbol();
        }
        cout << endl;
    }
} /// displayGrid

void timeStep(Bug *grid[][GRID_SIZE]) {
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 20; ++j) {
            if (grid[i][j] != nullptr)
                if (grid[i][j]->getSymbol() == '&') {
                    grid[i][j]->move(grid);
                    if (grid[i][j]->getSteps() >= 8)
                        grid[i][j]->breed(grid);
                }
        }
    }
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 20; ++j) {
            if (grid[i][j] != nullptr)
                if (grid[i][j]->getSymbol() == ';') {
                    grid[i][j]->move(grid);
                    if (grid[i][j]->getSteps() >= 3)
                        grid[i][j]->breed(grid);
                }
        }
    }
} ///timeStep
