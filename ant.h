/**
 *   @file: ant.h
 * @author: Nasseef Abukamail
 *   @date: July 28, 2021
 *  @brief: Ant class
 */

#ifndef ANT_H
#define ANT_H

#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "bug.h"
using namespace std;

class Ant : public Bug {
   public:
    Ant();

    Ant(size_t row, size_t col);

    ~Ant();

    void move(Bug* grid[][GRID_SIZE]) override;
    void breed(Bug* grid[][GRID_SIZE]) override;
};

#endif  // ANT_H
